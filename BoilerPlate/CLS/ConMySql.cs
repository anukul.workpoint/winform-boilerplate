﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinFormBoilerPlate.CLS
{
    class ConMySql
    {
        private static String strConnect = "server=127.0.0.1;port = 3306;user id=" + "" + ";password=" + "" + ";database=test;Character Set =utf8 ;default command timeout=3600;";

        private MySqlConnection conn;
        private MySqlTransaction mTransaction;
        private bool mIsInTransaction;
        private bool disposedValue;

        public void checkConnect()
        {

            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }
            string sql = null;
            sql = "SET NAMES UTF8;";
            try
            {
                using (MySqlCommand cmd = new MySqlCommand(sql, this.conn))
                {
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                conn.Close();
                conn.Open();
            }
        }

        public ConMySql()
        {
            conn = new MySqlConnection(strConnect);
            mIsInTransaction = false;

            try
            {
                conn.Close();
                conn.Open();
            }
            catch (MySqlException ex)
            {
                if (ex.Number == 1042)
                {
                    //MessageBox.Show("ไม่สามารถเชื่อมต่อกับ Server โปรดตรวจสอบระบบ Network " + ex.ToString());
                    //MessageBox.Show("ไม่สามารถเชื่อมต่อกับ Server โปรดตรวจสอบระบบ Network ");
                    return;
                }
                //MessageBox.Show(ex.ToString());
                conn.Close();

                conn.Open();

            }
        }

        #region "dispose"

        protected void Dispose(bool disposing)
        {
            if (!this.disposedValue)
            {
                if (disposing)
                {
                }

                if ((mIsInTransaction))
                {
                    mTransaction.Rollback();
                    mTransaction.Dispose();
                }
                conn.ClearAllPoolsAsync();

                conn.Close();
                conn.Dispose();

            }
            this.disposedValue = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion


        public DataTable getTable(string sql)
        {
            this.checkConnect();
            DataTable dt = new DataTable();

            if ((this.mIsInTransaction))
            {
                using (MySqlCommand selectCommand = new MySqlCommand(sql, this.conn, this.mTransaction))
                {
                    using (MySqlDataAdapter adapter = new MySqlDataAdapter(selectCommand))
                    {
                        adapter.Fill(dt);
                        adapter.Dispose();
                    }
                    selectCommand.Dispose();
                }
            }
            else
            {
                using (MySqlDataAdapter adapter = new MySqlDataAdapter(sql, this.conn))
                {
                    adapter.Fill(dt);
                    adapter.Dispose();

                }
            }
            return dt;
        }


        public void getTable(string sql, ref DataTable dt)
        {
            this.checkConnect();

            if ((this.mIsInTransaction))
            {
                using (MySqlCommand selectCommand = new MySqlCommand(sql, this.conn, this.mTransaction))
                {
                    using (MySqlDataAdapter adapter = new MySqlDataAdapter(selectCommand))
                    {
                        adapter.Fill(dt);
                        adapter.Dispose();
                    }
                    selectCommand.Dispose();
                }
            }
            else
            {
                using (MySqlCommand selectCommand = new MySqlCommand(sql, this.conn))
                {
                    using (MySqlDataAdapter adapter = new MySqlDataAdapter(selectCommand))
                    {
                        adapter.Fill(dt);
                        adapter.Dispose();
                    }
                    selectCommand.Dispose();
                }

            }
        }

        #region "transaction"


        public void BeginTrans()
        {
            this.checkConnect();
            if ((!mIsInTransaction))
            {
                mTransaction = conn.BeginTransaction();
                mIsInTransaction = true;
            }
        }

        public void RollbackTrans()
        {
            if ((mIsInTransaction))
            {
                mTransaction.Rollback();
                mTransaction.Dispose();
                mIsInTransaction = false;
            }
        }

        public void CommitTrans()
        {
            if ((mIsInTransaction))
            {
                mTransaction.Commit();
                mTransaction.Dispose();
                mIsInTransaction = false;
            }
        }

        #endregion


        public int ExecuteNonQuery(string sql)
        {
            this.checkConnect();
            int result = 0;
            if ((this.mIsInTransaction))
            {
                using (MySqlCommand cmd = new MySqlCommand(sql, this.conn, this.mTransaction))
                {
                    result = cmd.ExecuteNonQuery();
                    cmd.Dispose();

                }
            }
            else
            {
                using (MySqlCommand cmd = new MySqlCommand(sql, this.conn))
                {
                    result = cmd.ExecuteNonQuery();

                    cmd.Dispose();
                }

            }
            return result;
        }


        public int ExecuteNonQuery(string sql, MySqlParameter[] mParameter)
        {
            //for BLOB object
            this.checkConnect();

            int result = 0;

            if ((this.mIsInTransaction))
            {
                MySqlCommand cmd = new MySqlCommand(sql, this.conn, this.mTransaction);

                for (int i = 0; i <= mParameter.Length - 2; i++)
                {
                    cmd.Parameters.Add(mParameter[i]);
                }

                result = cmd.ExecuteNonQuery();
                cmd.Dispose();


            }
            else
            {
                MySqlCommand cmd = new MySqlCommand(sql, this.conn);

                for (int i = 0; i <= mParameter.Length - 2; i++)
                {
                    cmd.Parameters.Add(mParameter[i]);
                }

                result = cmd.ExecuteNonQuery();
                cmd.Dispose();

            }
            return result;

        }


        public object ExecuteScalar(string sql)
        {
            this.checkConnect();

            object result = null;

            if ((this.mIsInTransaction))
            {
                using (MySqlCommand cmd = new MySqlCommand(sql, this.conn, this.mTransaction))
                {
                    result = cmd.ExecuteScalar();
                    cmd.Dispose();

                }
            }
            else
            {
                using (MySqlCommand cmd = new MySqlCommand(sql, this.conn))
                {
                    result = cmd.ExecuteScalar();
                    cmd.Dispose();

                }
            }
            return result;
        }

        public void addErrorlog(string sql, string err)
        {
            string y = "";
            string z = "";

            y += "sql, ";
            z += "'" + MySqlHelper.EscapeString(sql) + "', ";

            y += "error, ";
            z += "'" + MySqlHelper.EscapeString(err) + "', ";

            y += "dadd";
            z += "NOW()";

            string x = "INSERT INTO errlog (" + y + ") VALUES (" + z + ");";

            this.ExecuteNonQuery(x);
        }

        public static string checkAndInsertNull(string data, Boolean comma)
        {
            if (string.IsNullOrWhiteSpace(data))
            {
                if (comma == true)
                {
                    return "null, ";
                }
                else
                {
                    return "null";
                }

            }
            else
            {

                if (comma == true)
                {
                    return "'" + MySqlHelper.EscapeString(data) + "', ";
                }
                else
                {
                    return "'" + MySqlHelper.EscapeString(data) + "'";
                }
            }
        }
    }
}