﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinFormBoilerPlate.CLS
{
    class ConPg
    {
        public static string connstring = String.Format("Server={0};Port={1};" + "User Id={2};Password={3};Database={4};Encoding={5};", "127.0.0.1", "5432", "postgres", "user", "pass", "Unicode");
        private NpgsqlConnection conn;

        public ConPg()
        {
            conn = new NpgsqlConnection(connstring);
            conn.Open();
        }

        public DataTable getTable(string sql)
        {
            DataTable dt = new DataTable();

            NpgsqlDataAdapter da = new NpgsqlDataAdapter(sql, conn);

            da.Fill(dt);

            return dt;
        }

        public void getTable(string sql, ref DataTable dt)
        {
            try
            {
                NpgsqlDataAdapter da = new NpgsqlDataAdapter(sql, conn);
                da.Fill(dt);
            }
            catch (Exception e)
            {

                MessageBox.Show("เกิดข้อผิดพลาดในการดึงข้อมูล" + Environment.NewLine + e, "", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }

        public void executeNonQuery(string sql)
        {
            NpgsqlCommand sqlExecute = new NpgsqlCommand(sql, conn);
            sqlExecute.ExecuteNonQuery();
        }

        public object executeScalar(string sql)
        {
            NpgsqlCommand sqlExecute = new NpgsqlCommand(sql, conn);
            object res = sqlExecute.ExecuteScalar();
            return res;
        }

        public void test()
        {
            // Making connection with Npgsql provider
            NpgsqlConnection conn = new NpgsqlConnection(connstring);
            conn.Open();

            // quite complex sql statement
            string sql = "SELECT * FROM f_comservice";
            // data adapter making request from our connection
            NpgsqlDataAdapter da = new NpgsqlDataAdapter(sql, conn);
            // i always reset DataSet before i do
            // something with it.... i don't know why :-)

            DataTable dt = new DataTable();

            da.Fill(dt);
            string name = "";
            //for (int i = 0; i < dt.Rows.Count; i++)
            //{
            //    name += dt.Rows[i]["description"].ToString();
            //}

            foreach (DataRow dr in dt.Rows)
            {
                name += dr["description"].ToString();
            }

            System.Windows.Forms.MessageBox.Show(name);

            conn.Close();
        }

        public void closeCon()
        {
            conn.Close();
        }
    }
}
